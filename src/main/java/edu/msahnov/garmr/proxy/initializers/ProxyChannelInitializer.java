package edu.msahnov.garmr.proxy.initializers;

import edu.msahnov.garmr.proxy.handlers.ProxyChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.ssl.SslContext;

public class ProxyChannelInitializer extends ChannelInitializer<SocketChannel> {

    private String backendHost;
    private int backendPort;
    private final SslContext sslCtx;
    private int maxConnections;

    public ProxyChannelInitializer(String host, int port, int maxConnections, SslContext sslCtx) {
        this.sslCtx = sslCtx;
        this.maxConnections = maxConnections;
        this.backendHost = host;
        this.backendPort = port;
    }

    /**
     *
     * @param ch
     * @throws Exception
     */
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline p = ch.pipeline();

        if (sslCtx != null) {
            p.addLast(sslCtx.newHandler(ch.alloc()));
        }
        p.addLast(new HttpRequestDecoder());
        p.addLast(new HttpServerCodec(102400, 102400, 102400));
        p.addLast(new ProxyChannelHandler(maxConnections, backendHost, backendPort));
    }
}