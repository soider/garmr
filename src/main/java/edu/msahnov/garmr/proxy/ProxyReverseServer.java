package edu.msahnov.garmr.proxy;

import edu.msahnov.garmr.proxy.initializers.ProxyChannelInitializer;
import edu.msahnov.garmr.util.PropertiesLoader;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.util.Properties;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import org.slf4j.LoggerFactory;

import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLException;


public class ProxyReverseServer {

    final static org.slf4j.Logger log = LoggerFactory.getLogger(ProxyReverseServer.class);

    private int PORT;
    private String HOST;
    private String BACKEND_HOST;
    private int BACKEND_PORT;

    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;


    private int bossGroupSize;
    private int workerGroupSize;

    private int maxConnectionsQueued;

    private boolean SSL;

    public ProxyReverseServer() {
        log.info("Begin initialization of ProxyReverseServer");
        Properties prop = PropertiesLoader.getProperties("proxy_config.properties");

        /* Backend Address */
        BACKEND_HOST = prop.getProperty("backend_host");
        BACKEND_PORT = Integer.parseInt(prop.getProperty("backend_port"));

        /* Frontend PORT */
        HOST = prop.getProperty("host");
        PORT = Integer.parseInt(prop.getProperty("port"));

        SSL = Boolean.parseBoolean(prop.getProperty("is_ssl"));

        bossGroupSize = Integer.parseInt(prop.getProperty("bossGroup"));
        workerGroupSize = Integer.parseInt(prop.getProperty("workerGroup"));

        maxConnectionsQueued = Integer.parseInt(prop.getProperty("connectionsQueued"));

        log.debug("Done reading configuration");
    }

    public void start() {
        log.debug("Starting the server");
        log.debug("Starting inbound http listener on PORT {}", PORT);

        // Configure SSL.
        SslContext sslCtx = null;
        if (SSL) {
            try {
                SelfSignedCertificate ssc = new SelfSignedCertificate();
                sslCtx = SslContext.newServerContext(ssc.certificate(), ssc.privateKey());
            } catch (CertificateException ex) {
                Logger.getLogger(ProxyReverseServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SSLException ex) {
                Logger.getLogger(ProxyReverseServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        bossGroup = new NioEventLoopGroup(bossGroupSize);
        workerGroup = new NioEventLoopGroup(workerGroupSize);

        try {
            ServerBootstrap b = new ServerBootstrap();

            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(HOST, PORT)
                    .childHandler(new ProxyChannelInitializer(BACKEND_HOST,
                            BACKEND_PORT,
                            maxConnectionsQueued,
                            sslCtx))
                    .childOption(ChannelOption.AUTO_READ, false);

            b.option(ChannelOption.TCP_NODELAY, true);
            b.childOption(ChannelOption.TCP_NODELAY, true);

            b.option(ChannelOption.SO_BACKLOG, maxConnectionsQueued);
            b.option(ChannelOption.SO_KEEPALIVE, true);
            b.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 15000);

            b.option(ChannelOption.SO_SNDBUF, 1048576);
            b.option(ChannelOption.SO_RCVBUF, 1048576);
            b.childOption(ChannelOption.SO_RCVBUF, 1048576);
            b.childOption(ChannelOption.SO_SNDBUF, 1048576);

            b.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);

            Channel ch = null;
            try {
                ch = b.bind(PORT).sync().channel();
                ch.closeFuture().sync();
                log.debug("Inbound listener started");
            } catch (InterruptedException e) {
                log.error("Exception caught", e);
            }
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) {
        ProxyReverseServer inboundHttpListner = new ProxyReverseServer();
        inboundHttpListner.start();
        log.info("ProxyReverseServer started");
    }
}