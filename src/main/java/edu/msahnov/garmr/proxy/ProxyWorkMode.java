package edu.msahnov.garmr.proxy;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import edu.msahnov.garmr.mongo.MongoConnection;
import edu.msahnov.garmr.util.PropertiesLoader;
import org.bson.Document;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by msahnov on 20.05.15.
 */

public enum ProxyWorkMode {
    NORMAL,
    ATTACK;
    private static Object mutex = new Object();


    public static ProxyWorkMode getCurrentMode(String mark) {
        MongoCollection<Document> modeCollection = MongoConnection
                .getInstance()
                .getCollection("workMode");
        Document currentMode = modeCollection.find(eq("mark", mark)).first();
        if (currentMode == null) {
            return ProxyWorkMode.NORMAL;
        }
        ProxyWorkMode returnMode = (currentMode.get("mode") == null) ?
                ProxyWorkMode.NORMAL :
                ProxyWorkMode.valueOf((String) currentMode.get("mode"));
        return returnMode;
    }

    private static ProxyWorkMode curMode = getCurrentMode(
            PropertiesLoader.getProperties("main.properties").getProperty("instance_name")
    );

    public static void setMode(ProxyWorkMode mode, String mark) {
        if (!curMode.equals(mode)) {
            MongoCollection<Document> modeCollection = MongoConnection
                    .getInstance()
                    .getCollection("workMode");
            Document filter = new Document("mark", mark);
            Document update = new Document("$set", new Document("mode", mode.toString()));
            UpdateOptions options = new UpdateOptions();
            options.upsert(true);
            modeCollection.updateOne(filter, update, options);
            curMode = mode;
        }
    }

    public static void setAttack(String mark) {
        synchronized (mutex) {
            setMode(ProxyWorkMode.ATTACK, mark);
        }

    }

    public static void setNormal(String mark) {
        synchronized (mutex) {
            setMode(ProxyWorkMode.NORMAL, mark);
        }
    }

}
