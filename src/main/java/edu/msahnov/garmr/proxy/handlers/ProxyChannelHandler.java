package edu.msahnov.garmr.proxy.handlers;

/**
 * Created by msahnov on 20.05.15.
 */
import edu.msahnov.garmr.accesses.AccessManager;
import edu.msahnov.garmr.accesses.AccessStatus;
import edu.msahnov.garmr.proxy.ProxyWorkMode;
import edu.msahnov.garmr.proxy.initializers.ProxyTargetChannelInitializer;
import edu.msahnov.garmr.util.PropertiesLoader;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

/**
 * actual event handling class for netty
 */

public class ProxyChannelHandler extends ChannelInboundHandlerAdapter {

    final static Logger log = LoggerFactory.getLogger(ProxyChannelHandler.class);

    private volatile Channel outboundChannel;
    private volatile Channel inboundChannel;
    private final int maxConnectionsQueued;

    private ChannelInboundHandler handler;

    private String backendHost;
    private int backendPort;
    private boolean inited;

    public ProxyChannelHandler(int maxConnectionsQueued, String host, int port) {
        this.maxConnectionsQueued = maxConnectionsQueued;
        this.backendHost = host;
        this.backendPort = port;
    }

    /**
     * activating registered handler to accept events.
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        inboundChannel = ctx.channel();

        if (ProxyWorkMode.getCurrentMode(PropertiesLoader
                .getProperties("main.properties")
                .getProperty("instance_name")) == ProxyWorkMode.ATTACK) {
            InetSocketAddress remoteAddress = (InetSocketAddress) inboundChannel.remoteAddress();
            log.debug("Checking for {} accesses", remoteAddress.getAddress().getHostAddress());
            AccessStatus status = AccessManager.getStatus(remoteAddress);
            if (status == AccessStatus.NORMAL) {
                log.debug("Normal");
                handleProxyRequest(ctx);
            }
            else if (status == AccessStatus.BAD) {
                log.debug("Banned");
                handleBannedRequest(ctx);
            }
            else if (status == AccessStatus.SUSPECTED) {
                log.debug("Suspect");
                handleSuspectedRequest(ctx);
            } else {
                log.debug("No status at all");
            }
        } else {
            handleProxyRequest(ctx);
        }
    }

    private void handleBannedRequest(ChannelHandlerContext ctx) {
        log.debug("Handle banned request");
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,
                HttpResponseStatus.FORBIDDEN);
        inboundChannel.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    private void handleSuspectedRequest(ChannelHandlerContext ctx) {
        log.debug("Handle suspect request");
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,
                HttpResponseStatus.FOUND);
        response.headers().add("Location", PropertiesLoader.getProperties("proxy_config.properties")
                .getProperty("garmr_web_base") + "/captcha");
        inboundChannel.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    private void handleProxyRequest(ChannelHandlerContext ctx) {
        createNormalHandler();
        Bootstrap b = new Bootstrap();
        b.group(inboundChannel.eventLoop()).channel(ctx.channel().getClass());
        b.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
        b.option(ChannelOption.TCP_NODELAY, true);
        b.option(ChannelOption.CONNECT_TIMEOUT_MILLIS,15000);
        b.option(ChannelOption.SO_SNDBUF, 1048576);
        b.option(ChannelOption.SO_RCVBUF, 1048576);
        b.handler(handler).option(ChannelOption.AUTO_READ, false);

        ChannelFuture f = b.connect(backendHost, backendPort);

        outboundChannel = f.channel();
        f.addListener(new ChannelFutureListener() {

            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    // connection complete start to read first data
                    inboundChannel.read();
                } else {
                    // Close the connection if the connection attempt has failed.
                    inboundChannel.close();
                }
            }
        });
    }

    private void createNormalHandler() {
        log.debug("Will use normal handler for this request");
        handler = new ProxyTargetChannelInitializer(inboundChannel);
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, Object msg) throws Exception {
        log.debug("Channel read");
        if (outboundChannel != null && outboundChannel.isActive()) {
            outboundChannel.writeAndFlush(msg).addListener(new ChannelFutureListener() {

                public void operationComplete(ChannelFuture future) throws Exception {
                    if (future.isSuccess()) {
                        // was able to flush out data, start to read the next chunk
                        ctx.channel().read();
                    } else {
                        future.channel().close();
                    }
                }
            });
        } else {
            log.debug("We have lost connection");
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        //  System.out.println("Channel Inactive");
        if (outboundChannel != null) {
            closeOnFlush(outboundChannel);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        //  System.out.println("Exception caught");
        cause.printStackTrace();
        closeOnFlush(ctx.channel());
    }

    /**
     * Closes the specified channel after all queued write requests are flushed.
     */
    static void closeOnFlush(Channel ch) {
        if (ch.isActive()) {
            ch.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
        }
    }
}