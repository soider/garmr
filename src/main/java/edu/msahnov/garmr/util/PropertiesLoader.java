package edu.msahnov.garmr.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;
import java.util.Scanner;


/**
 * Created by msahnov on 20.05.15.
 */

public class PropertiesLoader {

    final static Logger log = LoggerFactory.getLogger(PropertiesLoader.class);

    public static Properties getProperties(String path) {
        ClassLoader classLoader = PropertiesLoader.class.getClassLoader();

        FileInputStream file = null;
        try {
            if (classLoader.getResource(path) == null) {
                log.error("Can't get resource {} from classLoader", path);
                return null;
            }
            InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
            Properties properties = new Properties();
            properties.load(in);
            return properties;
        } catch (FileNotFoundException e) {
            log.error("Property file {} not found with exception: \n {}",
                            path,
                            e.getMessage()
                    );
        } catch (IOException e) {
            log.error("Can't load property file {} with exception: \n {}",
                            path,
                            e.getMessage()
                    );
        }
        return null;
    }
}
