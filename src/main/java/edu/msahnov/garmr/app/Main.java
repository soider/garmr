package edu.msahnov.garmr.app;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import edu.msahnov.garmr.analyze.TCPPacketWindow;
import edu.msahnov.garmr.mongo.MongoConnection;
import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;


/**
 * Created by msahnov on 20.05.15.
 */
public class Main {

    final static Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] argv) {
        System.out.println("Garmr app management tool");
        MongoCollection<Document> collection = MongoConnection.getInstance().getCollection("tcp4_log");
        ArrayList<TCPPacketMeta> elements = new ArrayList<>();
        MongoCursor<Document> cursor = collection.find().limit(100).iterator();
        try {
            while (cursor.hasNext()) {
                elements.add(TCPPacketMeta.fromBson(cursor.next()));
            }
        }
        finally {
            cursor.close();
        }

        TCPPacketWindow window = new TCPPacketWindow(elements);



    }
}
