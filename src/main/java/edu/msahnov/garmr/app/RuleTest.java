package edu.msahnov.garmr.app;

import edu.msahnov.garmr.analyze.DataNormalizer;
import edu.msahnov.garmr.analyze.Rule;
import edu.msahnov.garmr.analyze.TCPPacketWindow;
import edu.msahnov.garmr.analyze.Utils;
import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.Normalizer;
import java.util.ArrayList;

/**
 * Created by msahnov on 30.05.15.
 */
public class RuleTest {

    final static Logger log = LoggerFactory.getLogger(RuleTest.class);


    public static void main(String[] argv) {
        Rule normalRule = Rule.loadRule("normal");
        ArrayList<TCPPacketMeta> packets = Utils.getDataSet(100, 0);
        ArrayList<TCPPacketMeta> packets2 = Utils.getDataSet(100, 11232);
        TCPPacketWindow window1 = new TCPPacketWindow(packets);
        TCPPacketWindow window2 = new TCPPacketWindow(packets2);


        DataNormalizer normalizer = new DataNormalizer();
        ArrayList<TCPPacketWindow> windows = new ArrayList<>();
        windows.add(window1);
        windows.add(window2);
        normalizer.setWindowsList(windows);
        normalizer.normalize(0, 1);
        window1 = normalizer.getWindowsList().get(0);
        window2 = normalizer.getWindowsList().get(1);

        log.debug("{}", normalRule.accepts(window1));
        log.debug("{}", normalRule.accepts(window2));

    }
}
