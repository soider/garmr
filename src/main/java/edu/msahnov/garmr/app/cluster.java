package edu.msahnov.garmr.app;

import edu.msahnov.garmr.analyze.TCPPacketWindow;
import edu.msahnov.garmr.analyze.Utils;
import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by msahnov on 25.05.15.
 */


public class cluster {

    final static Logger log = LoggerFactory.getLogger(cluster.class);
    private static ArrayList<TCPPacketWindow> windowsList;





    public static void main (String[] argv) {
        final int WINDOW_SIZE=100;



        ArrayList<TCPPacketMeta> packetsMeta1 = Utils.getDataSet(90000, 0);
        Collections.shuffle(packetsMeta1);
        ArrayList<ArrayList<TCPPacketMeta>> packetsForWindow1 = new ArrayList<>();
        int packetCounter= 0;
        ArrayList<TCPPacketMeta> tmpList = new ArrayList<>();
        for (TCPPacketMeta packet: packetsMeta1) {
            tmpList.add(packet);
            packetCounter++;
            if (packetCounter==10) {
                packetsForWindow1.add(tmpList);
                tmpList = new ArrayList<>();
                packetCounter = 0;
            }
        }

        windowsList = new ArrayList<>();
        for (ArrayList<TCPPacketMeta> packets: packetsForWindow1) {
            windowsList.add(new TCPPacketWindow(packets));
        }

        KMeansPlusPlusClusterer<TCPPacketWindow> clusterer = new KMeansPlusPlusClusterer<TCPPacketWindow>(2);
        List<CentroidCluster<TCPPacketWindow>> cluster;
        cluster = clusterer.cluster(windowsList);

        CentroidCluster<TCPPacketWindow> first  = cluster.get(0);
        CentroidCluster<TCPPacketWindow> second = cluster.get(1);
        HashMap<Boolean, Integer> firstPortCounter = new HashMap<>();
        HashMap<Boolean, Integer> secondPortCounter = new HashMap<>();

        firstPortCounter.put(true, 0);
        firstPortCounter.put(false, 0);

        secondPortCounter.put(true, 0);
        secondPortCounter.put(false, 0);


        for (TCPPacketWindow window: first.getPoints()) {
            for (TCPPacketMeta packet: window.getPackets()) {
                firstPortCounter.put(packet.isSyn(),
                        firstPortCounter.get(packet.isSyn()) + 1);
            }
        }

        for (TCPPacketWindow window: second.getPoints()) {
            for (TCPPacketMeta packet: window.getPackets()) {
                secondPortCounter.put(packet.isSyn(),
                        secondPortCounter.get(packet.isSyn()) + 1);
            }
        }

//        log.debug("Я СДЕЛЯЛЬ");

    }
}
