package edu.msahnov.garmr.app;

import edu.msahnov.garmr.analyze.DataNormalizer;
import edu.msahnov.garmr.analyze.TCPPacketWindow;
import edu.msahnov.garmr.analyze.Utils;
import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;

import javax.xml.crypto.Data;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by msahnov on 28.05.15.
 */
public class window {

    public static void main(String[] argv) {

        TCPPacketMeta packet1, packet2;

        ArrayList<TCPPacketMeta> packets1 = Utils.getDataSet(100, 0);
        ArrayList<TCPPacketMeta> packets2 = Utils.getDataSet(10, 0);

        TCPPacketWindow window = new TCPPacketWindow(packets1);

        TCPPacketWindow window2 = new TCPPacketWindow(packets2);


        DataNormalizer norm = new DataNormalizer();
        ArrayList<TCPPacketWindow> windows = new ArrayList<>();
        windows.add(window);
        windows.add(window2);
        norm.setWindowsList(windows);
        norm.normalize(0, 1);
        System.out.println("Okay");
    }
}
