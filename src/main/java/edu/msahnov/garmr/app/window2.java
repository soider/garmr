package edu.msahnov.garmr.app;

import edu.msahnov.garmr.analyze.TCPPacketWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by msahnov on 29.05.15.
 */
public class window2 {

    final static Logger log = LoggerFactory.getLogger(window2.class);

    public static void main(String[] argv) {
        TCPPacketWindow window1, window2;

        window1 = new TCPPacketWindow();
        window2 = new TCPPacketWindow();

        window1.setPoint(new double[] {
                0.0,
                0.0,
                0.0
        });

        window2.setPoint(new double[] {
                0.0,
                0.0,
                0.0
        });

        log.debug("window1 is {} that window2", window1.compareTo(window2));

        window2.setPoint(new double[] {
                0.0,
                0.1,
                0.0
        });

        log.debug("window1 is {} that window2", window1.compareTo(window2));

        window2.setPoint(new double[] {
                0.0,
                -0.1,
                0.0
        });

        log.debug("window1 is {} that window2", window1.compareTo(window2));
    }
}
