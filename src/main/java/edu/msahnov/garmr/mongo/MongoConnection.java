package edu.msahnov.garmr.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.async.SingleResultCallback;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import edu.msahnov.garmr.util.PropertiesLoader;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

import static com.mongodb.client.model.Filters.*;


/**
 * Created by msahnov on 21.05.15.
 */


public class MongoConnection {
    final static Logger log = LoggerFactory.getLogger(MongoConnection.class);

    private final MongoDatabase database;
    private static MongoConnection instance;

    private MongoConnection(MongoDatabase database) {
        this.database = database;
    }

    public static MongoConnection getInstance() {
        Properties properties = PropertiesLoader.getProperties("mongo.properties");
        MongoClientURI connectionString = new MongoClientURI(
                properties.getProperty("mongo_connection_string")
        );
        if (instance == null) {
            log.debug("MongoConnection is not initialized, creating");
            MongoClient client = new MongoClient(connectionString);
            instance = new MongoConnection(
                    client.getDatabase(properties.getProperty("mongo_database"))
            );
        }
        return instance;
    }

    public MongoCollection<Document> getCollection(String collectionName) {
        return database.getCollection(collectionName);
    }

}
