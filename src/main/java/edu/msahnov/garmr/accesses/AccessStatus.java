package edu.msahnov.garmr.accesses;

/**
 * Created by msahnov on 21.05.15.
 */
public enum AccessStatus {
    NORMAL,
    BAD,
    SUSPECTED,
    HUMAN
};

