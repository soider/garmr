package edu.msahnov.garmr.accesses;

import com.mongodb.BasicDBObject;
import com.mongodb.Mongo;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.UpdateOptions;
import edu.msahnov.garmr.mongo.MongoConnection;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import static com.mongodb.client.model.Filters.*;


/**
 * Created by msahnov on 20.05.15.
 */


public class AccessManager {
    private static Object mutex = new Object();
    final static Logger log = LoggerFactory.getLogger(AccessManager.class);

    private static MongoCollection<Document> getCollection() {
        return MongoConnection
                .getInstance()
                .getCollection("addresses");
    }

    public static AccessStatus getStatus(final InetSocketAddress address) {
        MongoCollection<Document> statusCollection = getCollection();
        String hostAddress = address.getAddress().getHostAddress();
        log.debug("Try to find status for {}", hostAddress);
        Document result = statusCollection.find(eq("address", hostAddress))
                .first();
        if (result == null) {
            log.debug("Nothing in status collection, mean normal");
            return AccessStatus.NORMAL;
        } else {
            log.debug("Found something");
            String status = (String) result.get("status");
            AccessStatus finalStatus = status == null ? null : AccessStatus.valueOf(status);
            if (status == null && finalStatus == null) {
                log.error("No status in collection for {}, do something!", hostAddress);
                return AccessStatus.SUSPECTED;
            }
            return finalStatus;
        }
    }


    private static void setStatus(final String address, AccessStatus status) {
        synchronized (mutex) {
            MongoCollection<Document> statusCollection = getCollection();
            UpdateOptions options = new UpdateOptions();
            options.upsert(true);
            statusCollection.updateOne(
                    new Document("address", address),
                    new Document("$set", new Document("status", status.toString())),
                    options
            );
        }
    }

    private static void setStatus(final InetSocketAddress address, AccessStatus status) {
        log.debug("Setting status {} for {}", status, address.toString());

        setStatus(address.getAddress().getHostAddress(), status);
    }

    public static void suspect(final InetSocketAddress address) {
        log.debug("Banning {}", address);
        setStatus(address,
                AccessStatus.SUSPECTED);
    }


    public static void suspect(final String address) {
        log.debug("Banning {}", address);
            MongoCollection<Document> statusCollection = getCollection();
            MongoCursor<Document> cursor = statusCollection.find(new Document("address", address)).iterator();
            Document currentStatus = null;
            while (cursor.hasNext()) {
                currentStatus = cursor.next();
            }
            cursor.close();
            String statusValue = currentStatus.getString("status");
            if (currentStatus == null || isHuman(address)) {
                setStatus(address,
                        AccessStatus.SUSPECTED);
            }
        }

    public static boolean isHuman(final String address) {
        MongoCollection<Document> humanCollection = MongoConnection.getInstance().getCollection("humans");
        MongoCursor<Document> cursor = humanCollection.find(
                new Document("address", address)
        ).iterator();
        try {
            return cursor.hasNext();
        }
        finally {
            cursor.close();
        }
    }

}
