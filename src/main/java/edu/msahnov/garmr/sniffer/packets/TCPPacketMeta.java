package edu.msahnov.garmr.sniffer.packets;

import org.bson.Document;
import org.pcap4j.packet.EthernetPacket;
import org.pcap4j.packet.IpV4Packet;
import org.pcap4j.packet.TcpPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

/**
 * Created by msahnov on 21.05.15.
 */
public class TCPPacketMeta extends AbstractPacketMeta {

    final static Logger log = LoggerFactory.getLogger(TCPPacketMeta.class);

    private static final String[] KEYS = {
                "srcIpInt",
                "dstPort",
                "syn",
                "fin",
                "protocol",
                "timestamp"
    };

    private final String srcIp;
    private final int srcIpInt;
    private final int srcPort;
    private final int dstPort;


    private final int protocol;
    private long timestamp;

    private final boolean syn;
    private final boolean fin;

    public boolean isSyn() {
        return syn;
    }

    public boolean isFin() {
        return fin;
    }

    public TCPPacketMeta(final String srcIp,
                         final int srcIpInt,
                         final int srcPort,
                         final int dstPort,
                         final int protocol,
                         final long timestamp,
                         boolean syn,
                         boolean fin) {
        this.srcIp = srcIp;

        this.srcPort = srcPort;
        this.dstPort = dstPort;
        this.srcIpInt = srcIpInt;
        this.protocol = protocol;
        this.timestamp = timestamp;
        this.syn = syn;
        this.fin = fin;
    }

    public int getSrcIpInt() {
        return srcIpInt;
    }

    public Object get(String key) {
        switch (key) {
            case "srcPort":
                return getSrcPort();
            case "dstPort":
                return getSrcPort();
            case "srcIp":
                return getSrcIp();
            case "srcIpInt":
                return getSrcIpInt();
            case "protocol":
                return getProtocol();
            case "syn":
                return isSyn();
            case "fin":
                return isFin();
            case "timestamp":
                return getTimestamp();
            default:
                throw new IllegalArgumentException("Unknown key");
        }
    }

    public String getSrcIp() {
        return srcIp;
    }

    public int getSrcPort() {
        return srcPort;
    }

    public int getDstPort() {
        return dstPort;
    }

    public int getProtocol() {
        return protocol;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public static String[] getKeys() {
        return KEYS;
    }

    public static TCPPacketMeta fromPCAPPacket(EthernetPacket packet) {
        String srcIp = packet.get(IpV4Packet.class).getHeader().getSrcAddr().getHostAddress();
        int srcPort = packet.get(TcpPacket.class).getHeader().getSrcPort().valueAsInt();
        int dstPort = packet.get(TcpPacket.class).getHeader().getDstPort().valueAsInt();
        int protocol = Integer.parseInt(packet.get(IpV4Packet.class).getHeader().getProtocol().valueAsString());
        boolean syn = packet.get(TcpPacket.class).getHeader().getSyn();
        boolean fin = packet.get(TcpPacket.class).getHeader().getFin();

        int srcIPInt= packet.get(IpV4Packet.class).getHeader().getSrcAddr().hashCode();

        return new TCPPacketMeta(srcIp, srcIPInt,
                srcPort,
                dstPort,
                protocol,
                new java.util.Date().getTime(),
                syn,
                fin);
    }

    public static TCPPacketMeta fromBson(Document bsonObject) {
        log.debug("{}", bsonObject);
        return new TCPPacketMeta(
                bsonObject.getString("srcAddress"),
                bsonObject.getInteger("srcAddressInt"),
                bsonObject.getInteger("srcPort"),
                bsonObject.getInteger("dstPort"),
                bsonObject.getInteger("protocol"),
                bsonObject.getLong("timestamp"),
                bsonObject.getBoolean("syn"),
                bsonObject.getBoolean("fin")
               );
    }
}
