package edu.msahnov.garmr.sniffer;

import edu.msahnov.garmr.sniffer.handlers.*;
import edu.msahnov.garmr.util.PropertiesLoader;
import org.pcap4j.core.*;
import org.pcap4j.core.BpfProgram.BpfCompileMode;
import org.pcap4j.packet.Packet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by msahnov on 21.05.15.
 */
public class Sniffer implements Runnable {

    final static Logger log = LoggerFactory.getLogger(Sniffer.class);
    final static int MAX_ERROR_COUNT = 10;

    final private String device;
    final private String filter;
    final private int id;
    final private int readTimeout;
    final private int snapLength;
    final private int bufferSize;



    private boolean running;
    private static int runningSniffersCount = 0;

    private static ExecutorService pool;
    private ArrayList<AbstractPacketHandler> packetHandlers;

    private PcapNetworkInterface networkInterface;
    private PcapHandle handle;



    public Sniffer (int id,
                    String device,
                    int readTimeout,
                    int snapLength,
                    int bufferSize,
                    String filter) {
        this.id = id;
        this.device = device;
        this.filter = filter;
        this.readTimeout = readTimeout;
        this.snapLength = snapLength;
        this.bufferSize = bufferSize;
        setRunning(true);
    }

    private void setRunning(boolean running) {
        this.running = running;
        if (running) {
            runningSniffersCount++;
        } else {
            runningSniffersCount--;
        }
    }

    public void initHandlers() {
        pool = Executors.newCachedThreadPool();
        packetHandlers = new ArrayList<>();
        packetHandlers.add(new ICMPDetectorHandler());
        packetHandlers.add(new TCPSynFloodDetectorHandler());
        packetHandlers.add(new TCPPacketAnalyzerHandler());
        packetHandlers.add(new AttackDetector());
        packetHandlers.add(new TCPPacketSaverHandler());
    }

    public void run() {
        log.debug("Start sniffer {}", id);
        int errorCount = 0;
        initPcap();
        initHandlers();

        while (running) {
            log.debug("Message from sniffer {}", device);
            Packet packet = null;
            try {
                packet = handle.getNextPacket();
            } catch (NotOpenException e) {
                e.printStackTrace();
                errorCount++;
                if (errorCount > MAX_ERROR_COUNT) {
                    log.error("Sniffer {} has been failing too much", id);
                    setRunning(false);
                }
            }
            if (packet == null) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            } else {
                    for (AbstractPacketHandler handler: packetHandlers) {
                    if (handler != null) {
                        Callable run = handler.getRunnableFor(packet);
                        if (run != null) {
                            pool.submit(handler.getRunnableFor(packet));
                        }
                        else {
                            log.debug("No runnable available for handler {} and packet {}", handler, packet);
                        }
                    }
                }
            }
        }
    }

    private void initPcap() {
        try {
            networkInterface = Pcaps.getDevByName(device);
            if (networkInterface == null) {
                log.warn("No such interface  {}", device);
                setRunning(false);
            } else {
                log.debug("Succesfully inited for {} {}",
                        networkInterface.getName(),
                        networkInterface.getDescription());
                 handle = new PcapHandle.Builder(networkInterface.getName())
                        .snaplen(snapLength)
                        .promiscuousMode(PcapNetworkInterface.PromiscuousMode.PROMISCUOUS)
                        .timeoutMillis(readTimeout)
                        .bufferSize(bufferSize)
                        .build();
                if (filter != null) {
                    handle.setFilter(filter,
                            BpfCompileMode.OPTIMIZE);
                }
            }
        } catch (PcapNativeException e) {
            handleException(e);
        } catch (NotOpenException e) {
            handleException(e);
        }
    }

    private void handleException(Throwable e) {
        log.debug("Problem with device {}", device);
        e.printStackTrace();
        setRunning(false);
    }

    public static void main(String[] argv) {

        Properties properties = PropertiesLoader.getProperties("sniffer.properties");
        final int readTimeout = Integer.parseInt(properties.getProperty("read_timeout"));
        final int snapLength = Integer.parseInt(properties.getProperty("snaplen"));
        final int bufferSize = Integer.parseInt(properties.getProperty("buffer_size"));
        final String devices = properties.getProperty("devices");

        String[] devicesArray = devices.split(" ", -1);
        ArrayList<Sniffer> sniffers = new ArrayList<>();
        int i = 0;
        for (String device: devicesArray) {
            sniffers.add(new Sniffer(
                    i++,
                    device,
                    readTimeout,
                    snapLength,
                    bufferSize,
                    // вернуть tcp  в фильтр
                    "((tcp or icmp) and inbound) and not (port 22) and not (src host 192.168.0.138) and not (src host 192.168.0.134) and not (src host 127.0.0.1)"
            ));
        }
        ArrayList<Thread> threads = new ArrayList<>();
        for (Sniffer sniffer: sniffers) {
            Thread thread = new Thread(sniffer);
            thread.setDaemon(true);
            thread.start();
            threads.add(thread);
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                log.debug("Shutdown in progress!");
                for (Sniffer sniffer: sniffers) {
                    sniffer.setRunning(false);
                }
            }
        });

        while (runningSniffersCount > 0) {
            try {
                Thread.sleep(500);
                for (Thread thread: threads) {
                    if (!thread.isAlive()) {
                        runningSniffersCount = 0;
                        log.error("SNIFFER THREAD FAILED WITH EXCEPTION");
                        log.error(thread.getStackTrace().toString());
                    }
                }
                log.debug("Threads {}", runningSniffersCount);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
