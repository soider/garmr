package edu.msahnov.garmr.sniffer.handlers;

import com.mongodb.client.MongoCollection;
import edu.msahnov.garmr.mongo.MongoConnection;
import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import org.bson.Document;

/**
 * Created by msahnov on 21.05.15.
 */
public class TCPPacketSaverHandler extends TCPAbstractPacketHandler {

    class TCPPacketSaverRunnable extends TCPAbstractPacketHandler.TCPAbstractHandlerRunnable {
        @Override
        public Object call() {
            handle(this.packet);
            return null;
        }
    }

    @Override
    public TCPAbstractHandlerRunnable getRunnableFor(TCPPacketMeta packet) {
        TCPPacketSaverRunnable runnable = new TCPPacketSaverRunnable();
        runnable.setPacket(packet);
        return runnable;
    }

    public void handle(TCPPacketMeta packet) {
        // Debug?
        MongoConnection connection = MongoConnection.getInstance();
        MongoCollection<Document> collection = connection.getCollection("tcp4_log");
        Document doc = new Document()
                .append("srcAddress", packet.getSrcIp())
                .append("srcAddressInt", packet.getSrcIpInt())
                .append("srcPort", packet.getSrcPort())
                .append("dstPort", packet.getDstPort())
                .append("protocol", packet.getProtocol())
                .append("syn", packet.isSyn())
                .append("fin", packet.isFin())
                .append("timestamp", packet.getTimestamp());
        collection.insertOne(doc);
    }
}
