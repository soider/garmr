package edu.msahnov.garmr.sniffer.handlers;

import edu.msahnov.garmr.accesses.AccessManager;
import edu.msahnov.garmr.analyze.AnalyzeResult;
import edu.msahnov.garmr.analyze.TCPPacketWindow;
import edu.msahnov.garmr.analyze.TCPWindowAnalyzer;
import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by msahnov on 22.05.15.
 */


public class TCPPacketAnalyzerHandler extends TCPAbstractPacketHandler {
    private ArrayList<TCPPacketMeta> packets;
    private TCPPacketWindow window;
    private ThreadLocal<ArrayList<TCPPacketMeta>> threadLocalList;
    private static TCPWindowAnalyzer analyzer = new TCPWindowAnalyzer();
    private class TCPPacketAnalyzerRunnable extends TCPAbstractPacketHandler.TCPAbstractHandlerRunnable {

        @Override
        public Object call()  {
            handle(this.packet);
            return null;
        }
    }

    public TCPPacketAnalyzerHandler() {
        packets = new ArrayList<>();
        threadLocalList = new ThreadLocal<>();
    }

    @Override
    public TCPAbstractPacketHandler.TCPAbstractHandlerRunnable getRunnableFor(TCPPacketMeta packet) {
        TCPPacketAnalyzerRunnable runnable = new TCPPacketAnalyzerRunnable();
        runnable.setPacket(packet);
        return runnable;
    }

    void  handle(TCPPacketMeta packet) {
        ArrayList<TCPPacketMeta> tmpList; // for swap
        synchronized (this) {
            packets.add(packet);
            if (packets.size() > TCPWindowAnalyzer.WINDOW_SIZE) {
                tmpList = packets;
                threadLocalList.set(tmpList);
                flushPackets();
            }
        }
        if (threadLocalList.get().size() != 0) {
            createPacketWindow();
            analyzePacketWindow();
        }
    }

    void flushPackets() {
        packets = new ArrayList<>();
    }
    void createPacketWindow() {
        window = new TCPPacketWindow(threadLocalList.get());
    }

    void analyzePacketWindow() {
        AnalyzeResult result = window.analyzeWith(analyzer);
        if (result.suspectedAsMaleficent()) {
            log.error("We are under attack!");
            markEveryAddressAsSuspected(window);
        } else {
            // Do anything?
        }
    }

    void markEveryAddressAsSuspected(TCPPacketWindow window) {
        InetSocketAddress address;
        for (TCPPacketMeta packet: window.getPackets()) {
            address = new InetSocketAddress(
                    packet.getSrcIp(),
                    (int) packet.getSrcPort()
            );
            AccessManager.suspect(
                    address
            );
        }
    }





}
