package edu.msahnov.garmr.sniffer.handlers;

import org.pcap4j.packet.Packet;

import java.util.concurrent.Callable;

/**
 * Created by msahnov on 21.05.15.
 */
public abstract class AbstractPacketHandler {

    public abstract Callable getRunnableFor(Packet packet);

    abstract protected class AbstractHandlerRunnable implements Callable {

    }
    abstract void handle(Packet packet);
}
