package edu.msahnov.garmr.sniffer.handlers;

import edu.msahnov.garmr.proxy.ProxyWorkMode;
import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import edu.msahnov.garmr.util.PropertiesLoader;
import org.pcap4j.packet.Packet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;


/**
 * Created by msahnov on 21.05.15.
 */

public class AttackDetector extends AbstractPacketHandler {

    final static Logger log = LoggerFactory.getLogger(TCPAbstractPacketHandler.class);

    int counter;
    int limit;
    long lastCallTime;

    private class TCPPacketCounterRunnable extends AbstractPacketHandler.AbstractHandlerRunnable {
        protected Packet packet;
        public void setPacket(Packet packet) {
            this.packet = packet;
        }

        @Override
        public Object call()  {
            handle(this.packet);
            return null;
        }
    }

    public AttackDetector() {
        counter = 0;
        lastCallTime = System.currentTimeMillis();
        try {
            limit = Integer.parseInt(
                    PropertiesLoader.getProperties("sniffer.properties")
                            .getProperty("attack_tcp_count")
            );
        } catch (NumberFormatException e) {
            limit = 4000;
        }
    }

    @Override
    public Callable getRunnableFor(Packet packet) {
        TCPPacketCounterRunnable runnable = new TCPPacketCounterRunnable();
        runnable.setPacket(packet);
        return runnable;
    }

    @Override
    public void handle(Packet packet) {
        synchronized (this) {
            counter ++;
            log.debug("Packets count {} time {}", counter, lastCallTime);
            if (System.currentTimeMillis() - lastCallTime >= 60000) {
                lastCallTime = System.currentTimeMillis();
                if (counter >= limit) {
                    log.debug("POSSIBLY WE ARE UNDER ATTACK");
                    ProxyWorkMode.setAttack(
                            PropertiesLoader.getProperties("main.properties")
                            .getProperty("instance_name")
                    );
                }
            }
        }
    }
}
