package edu.msahnov.garmr.sniffer.handlers;

import edu.msahnov.garmr.accesses.AccessManager;
import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import org.pcap4j.packet.*;

import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by msahnov on 30.05.15.
 */
public class ICMPDetectorHandler extends  AbstractPacketHandler {
    static HashMap<String, Boolean> schedules = new HashMap<>();

    class ICMPDetectorHandlerRunnable extends AbstractHandlerRunnable {

        private Packet packet;

        public ICMPDetectorHandlerRunnable(Packet packet) {
            this.packet = packet;
        }

        @Override
        public Object call() {
            handle(packet);
            return null;
        }
    }

    @Override
    public Callable getRunnableFor(Packet packet) {
        IcmpV4CommonPacket icmpPacket = packet.get(IcmpV4CommonPacket.class);
        if (icmpPacket != null) {
            return new ICMPDetectorHandlerRunnable(packet);
        }
        return null;
    }

    @Override
    public void handle(Packet packet) {
        synchronized (this) {
            String address = packet.get(IpV4Packet.class).getHeader().getSrcAddr().getHostAddress();
            if (schedules.get(address) == null || !schedules.get(address)) {
                final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
                executor.schedule(() -> AccessManager.suspect(address), 15, TimeUnit.SECONDS);
            }
        }
    }
}
