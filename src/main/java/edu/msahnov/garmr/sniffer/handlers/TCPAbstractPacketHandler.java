package edu.msahnov.garmr.sniffer.handlers;

import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import org.pcap4j.packet.EthernetPacket;
import org.pcap4j.packet.IpV4Packet;
import org.pcap4j.packet.Packet;
import org.pcap4j.packet.TcpPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

/**
 * Created by msahnov on 21.05.15.
 */
public abstract class TCPAbstractPacketHandler extends AbstractPacketHandler {

    protected abstract class TCPAbstractHandlerRunnable extends AbstractPacketHandler.AbstractHandlerRunnable {
        protected TCPPacketMeta packet;

        public void setPacket(TCPPacketMeta packet) {
            this.packet = packet;
        }
        abstract public Object call();
    }

    final static Logger log = LoggerFactory.getLogger(TCPAbstractPacketHandler.class);

    @Override
    public void handle(Packet packet) {
        TCPPacketMeta meta = TCPPacketMeta.fromPCAPPacket((EthernetPacket) packet);
        handle(meta);
    }

    @Override
    public Callable getRunnableFor(Packet packet) {
        TcpPacket tcpPacket = packet.get(TcpPacket.class);
        if (tcpPacket != null) {
            TCPPacketMeta meta = TCPPacketMeta.fromPCAPPacket((EthernetPacket) packet);
            return getRunnableFor(meta);
        }
        return null;
    }

    abstract public TCPAbstractHandlerRunnable getRunnableFor(TCPPacketMeta packet);

    abstract void handle(TCPPacketMeta packet);
}
