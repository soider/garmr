package edu.msahnov.garmr.sniffer.handlers;

import edu.msahnov.garmr.accesses.AccessManager;
import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


class SynCheck {
    final static Logger log = LoggerFactory.getLogger(SynCheck.class);
    final long MINUTE = 60000;
    static  HashMap<String, Integer> synCounter = new HashMap<>();
    static HashMap<String, Integer> finCounter = new HashMap<>();
    static HashMap<String, Long> lastCheck = new HashMap<>();

    static SynCheck instance = null;

    private SynCheck() {

    }

    public static SynCheck getInstance() {
        if (instance == null) {
            instance = new SynCheck();
        }
        return instance;
    }

    void incrementSyn(String address) {
        Integer currentValue = synCounter.get(address);
        if (currentValue != null) {
            currentValue++;
        } else {
            currentValue = 1;
        }
        synCounter.put(address, currentValue);
    }
    void incrementFin(String address) {
        Integer currentValue = finCounter.get(address);
        if (currentValue != null) {
            currentValue++;
        } else {
            currentValue = 1;
        }
        finCounter.put(address, currentValue);
    }

    void flushCounters(String address) {
        finCounter.put(address, 0);
        synCounter.put(address, 0);
    }

    boolean check(String address, boolean syn, boolean fin) {
        if (syn) {
            incrementSyn(address);
        }
        if (fin) {
            incrementFin(address);
        }
        Long nowTs = System.currentTimeMillis();
        Long lastTs = lastCheck.get(address);
        if (lastTs == null) { // first time check
            log.debug("SYN First time syn check for {}", address);
            lastCheck.put(address, nowTs);
            return true;
        } else {
            if (nowTs - lastTs >= MINUTE) {
                log.debug("SYN Flushing all for {}", address);
                lastCheck.put(address, nowTs);
                flushCounters(address);
                return true;
            } else {
                log.debug("SYN Checking address {} for flood", address);
                Integer synValue, finValue;
                synValue = synCounter.get(address);
                finValue = finCounter.get(address);
                if (synValue == null) {
                    synValue = 0;
                }
                if (finValue == null) {
                    finValue = 1;
                }
                double ratio = (double) synValue / finValue;

                if ( ratio > 100) {
                    log.debug("SYN FLOOD DETEecTEd FOR {}", address);
                    return false;
                }
                return true;
            }
        }
    }
}

/**
 * Created by msahnov on 30.05.15.
 */
public class TCPSynFloodDetectorHandler extends  TCPAbstractPacketHandler{

    static HashMap<String, Boolean> schedules = new HashMap<>();

    class TCPSynFloodDetectorRunnable extends TCPAbstractPacketHandler.TCPAbstractHandlerRunnable {

        @Override
        public Object call() {
            handle(this.packet);
            return null;
        }
    }

    @Override
    public TCPAbstractHandlerRunnable getRunnableFor(TCPPacketMeta packet) {
        TCPSynFloodDetectorRunnable runnable =  new TCPSynFloodDetectorRunnable();
        runnable.setPacket(packet);
        return runnable;
    }


    @Override
    void handle(TCPPacketMeta packet) {

        synchronized (this) {
            if (!SynCheck.getInstance().check(packet.getSrcIp(), packet.isSyn(), packet.isFin())) {
                if (schedules.get(packet.getSrcIp()) == null || !schedules.get(packet.getSrcIp())) {
                    final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
                    executor.schedule(() -> AccessManager.suspect(packet.getSrcIp()), 15, TimeUnit.SECONDS);
                    schedules.put(packet.getSrcIp(), true);
                } else {
                    log.debug("Already scheduled ban for {}");
                }
            };
        }
    }
}
