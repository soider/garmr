package edu.msahnov.garmr.analyze;

import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by msahnov on 22.05.15.
 */
public class TCPPacketWindow implements Clusterable, Comparable<TCPPacketWindow> {

    final static Logger log = LoggerFactory.getLogger(TCPPacketWindow.class);

    private double[] point;
    private ArrayList<TCPPacketMeta> packets;
    public HashMap<String, Double> fieldsEntropy;
    private HashMap<String,
            HashMap<Object, Integer>
            > occurenceCounters;

    public TCPPacketWindow(ArrayList<TCPPacketMeta> packets) {

        this.packets = packets;

        initBeginCounters();
        fillCounters();
        calculateWindowData();
    }

    public TCPPacketWindow() {
        log.debug("Created empty window");
    }

    void initBeginCounters() {
        occurenceCounters = new HashMap<>();
        fieldsEntropy = new HashMap<>();
        for (String key: TCPPacketMeta.getKeys()) {
            occurenceCounters.put(key, new HashMap<>());
            fieldsEntropy.put(key, (double) 0);
        }
    }

    void fillCounters() {
        for (TCPPacketMeta packet: packets) {
            for (String key: TCPPacketMeta.getKeys()) {
                Object currentPacketValue = packet.get(key);
                HashMap<Object, Integer> currentFieldCounter;
                currentFieldCounter = occurenceCounters.get(key);
                Integer currentCount = currentFieldCounter.get(currentPacketValue);
                if (currentCount == null) {
                    currentFieldCounter.put(currentPacketValue, 1);
                } else {
                    currentFieldCounter.put(currentPacketValue, currentCount + 1);
                }
            }
        }
    }

    void calculateWindowData() {
        Double probability;
        for (String key: TCPPacketMeta.getKeys()) {
            Set<Object> keySet = occurenceCounters.get(key).keySet();
            for (Object value: keySet) {
                Integer valueOccursTimes = occurenceCounters.get(key).get(value);
                probability = ((double) valueOccursTimes / getWindowSize());
                double currentEntropy = fieldsEntropy.get(key);
                currentEntropy = currentEntropy - (probability * Math.log(probability));
                fieldsEntropy.put(key, currentEntropy);
                log.debug("!!! {} probability is {}", key, probability);
            }
        }
        point = new double[] {
                //fieldsEntropy.get("srcIpInt"),
                //fieldsEntropy.get("dstPort") / 100,
                fieldsEntropy.get("syn"),
                fieldsEntropy.get("fin")
        };
    }

    public int getWindowSize() {
        return packets.size();
    }

    public ArrayList<TCPPacketMeta> getPackets() {
        return packets;
    }

    public AnalyzeResult analyzeWith(TCPWindowAnalyzer analyzer) {
        return analyzer.analyze(this);
    }

    public void setPoint(double[] point) {
        this.point = point;
    }

    @Override
    public double[] getPoint() {
        return point;
    }

    @Override
    public int compareTo(TCPPacketWindow o) {
        double[] myPoint = getPoint();
        double[] otherPoint = o.getPoint();
        int result = 0;
        for (int i = 0; i < myPoint.length; i++) {
            if (myPoint[i] == otherPoint[i]) {
                continue;
            } else if (myPoint[i] < otherPoint[i]) {
                result = -1;
                break;
            } else {
                result = 1;
                break;
            }

        }
        return result;
    }
}
