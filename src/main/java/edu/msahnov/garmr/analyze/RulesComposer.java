package edu.msahnov.garmr.analyze;

import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.ml.distance.EuclideanDistance;
import org.apache.commons.math3.util.FastMath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by msahnov on 30.05.15.
 */
public class RulesComposer {

    final static Logger log = LoggerFactory.getLogger(RulesComposer.class);
    private ArrayList<TCPPacketWindow> windowsList;
    List<CentroidCluster<TCPPacketWindow>> clusters;
    CentroidCluster<TCPPacketWindow> biggest, smallest;


    public void compose() {
        getRawData();
        //normalize();
        cluster();
    }

    double[][] getRules() {
        java.util.ArrayList<double[]> rules = new ArrayList<>();
        return new double[][] {
                clusters.get(0).getCenter().getPoint(),
                clusters.get(1).getCenter().getPoint()
        };
    }
    void getRawData() {
        ArrayList<TCPPacketMeta> packets = Utils.getDataSet(100000, 0);
        ArrayList<TCPPacketMeta> tmpList = new ArrayList<>();
        windowsList = new ArrayList<>();
        Collections.shuffle(packets);
        windowsList = new ArrayList<>();
        int packetCounter = 0;
        for (TCPPacketMeta packet: packets) {
            tmpList.add(packet);
            packetCounter++;
            if (packetCounter > TCPWindowAnalyzer.WINDOW_SIZE) {
                windowsList.add(new TCPPacketWindow(tmpList));
                tmpList = new ArrayList<>();
                packetCounter = 0;
            }
        }
    }

    void normalize() {
        DataNormalizer normalizer = new DataNormalizer();
        normalizer.setWindowsList(windowsList);
        normalizer.normalize(0, 1);
        windowsList = normalizer.getWindowsList();
    }

    void cluster() {
        KMeansPlusPlusClusterer<TCPPacketWindow> clusterer = new KMeansPlusPlusClusterer<TCPPacketWindow>(2);
        clusters = clusterer.cluster(windowsList);
    }

    double distance(double[] p1, double[] p2) {
        double sum = 0.0D;

        for(int i = 0; i < p1.length; ++i) {
            double dp = p1[i] - p2[i];
            sum += dp * dp;
        }

        return FastMath.sqrt(sum);
    }

    TCPPacketWindow findMinPoint(CentroidCluster<TCPPacketWindow> cluster) {
        double[] center = cluster.getCenter().getPoint();
        TCPPacketWindow fakeCenter = new TCPPacketWindow();
        fakeCenter.setPoint(center);
        ArrayList<TCPPacketWindow> packets = (ArrayList<TCPPacketWindow>) cluster.getPoints();
        TCPPacketWindow minimum = fakeCenter;
        double currentDistance = 0.0D;
        for (TCPPacketWindow packet: packets) {
            double dist = distance(center, packet.getPoint());
            if (dist <= currentDistance) {
                currentDistance = dist;
                minimum = packet;
            }

        }
        return minimum;
    }

    TCPPacketWindow findMaxPoint(CentroidCluster<TCPPacketWindow> cluster) {
        double[] center = cluster.getCenter().getPoint();
        TCPPacketWindow fakeCenter = new TCPPacketWindow();
        fakeCenter.setPoint(center);
        ArrayList<TCPPacketWindow> packets = (ArrayList<TCPPacketWindow>) cluster.getPoints();
        TCPPacketWindow maximum = fakeCenter;
        double currentDistance = 0.0D;
        for (TCPPacketWindow packet: packets) {
            double dist = distance(center, packet.getPoint());
            if (dist >= currentDistance) {
                currentDistance = dist;
                maximum = packet;
            }
        }
        return maximum;
    }

    void findBiggestCluster() {
        biggest = clusters.get(0);
        for (CentroidCluster<TCPPacketWindow> cluster: clusters) {
            if (cluster.getPoints().size() > biggest.getPoints().size()) {
                biggest = cluster;
            }
        }
    }

    void findSmallestCluster() {
        smallest = clusters.get(0);
        for (CentroidCluster<TCPPacketWindow> cluster: clusters) {
            if (cluster.getPoints().size() < biggest.getPoints().size()) {
                smallest = cluster;
            }
        }
    }

    public static void main(String[] argv) {
        RulesComposer composer = new RulesComposer();
        composer.compose();
        composer.findBiggestCluster();
        composer.findBiggestCluster();
        composer.findSmallestCluster();
        TCPPacketWindow biggestMin, biggestMax, smallestMin, smallestMax;
        biggestMax = composer.findMaxPoint(composer.biggest);
        biggestMin = composer.findMinPoint(composer.biggest);

        smallestMax = composer.findMaxPoint(composer.smallest);
        smallestMin = composer.findMinPoint(composer.smallest);
        log.debug("123");

//        Rule.saveRule("normal",
//                biggestMin.getPoint(),
//                biggestMax.getPoint());

//        Rule.saveRule("anomaly",
//                smallestMin.getPoint(),
//                smallestMax.getPoint()
//                );
    }
}
