package edu.msahnov.garmr.analyze;

/**
 * Created by msahnov on 22.05.15.
 */
public class AnalyzeResult {
    public static int counter = 0; // debug mode
    private boolean isMaleficent;

    public AnalyzeResult(boolean isMaleficent) {
        this.isMaleficent = isMaleficent;
    }

    public boolean suspectedAsMaleficent() {
        return isMaleficent;
    }
}
