package edu.msahnov.garmr.analyze;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by msahnov on 22.05.15.
 */
public class TCPWindowAnalyzer {

    public final static int WINDOW_SIZE = 100;

    final static Logger log = LoggerFactory.getLogger(TCPWindowAnalyzer.class);

    AnalyzeResult analyze(TCPPacketWindow window) {
        log.debug("Will analyze {}", window);
        Rule rule = Rule.loadRule("normal");
        return new AnalyzeResult(rule.accepts(window));
    }
}
