package edu.msahnov.garmr.analyze;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import edu.msahnov.garmr.mongo.MongoConnection;
import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import org.bson.Document;

import java.util.ArrayList;

import static com.mongodb.client.model.Filters.*;

/**
 * Created by msahnov on 25.05.15.
 */

public class Utils {
    public static ArrayList<TCPPacketMeta> getDataSet(int count, int skip) {
        ArrayList<TCPPacketMeta> res = new ArrayList<>();

        MongoCollection collection = MongoConnection.getInstance().getCollection("tcp4_log");

        MongoCursor<Document> cursor = collection.find().limit(count)
                .skip(skip)
                .iterator();

        try {
            while (cursor.hasNext()) {
                res.add(TCPPacketMeta.fromBson(cursor.next()));
            }
        } finally {
            cursor.close();
        }

        return res;
    }
}
