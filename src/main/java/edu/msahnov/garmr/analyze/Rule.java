package edu.msahnov.garmr.analyze;

import com.mongodb.BasicDBList;
import com.mongodb.client.MongoCursor;
import edu.msahnov.garmr.mongo.MongoConnection;
import org.apache.commons.lang3.ArrayUtils;
import org.bson.Document;

/**
 * Created by msahnov on 30.05.15.
 */
public class Rule {

    private double[] min, max;

    public double[] getMin() {
        return min;
    }

    public double[] getMax() {
        return max;
    }

    public Rule(double[] min, double[] max) {
        this.min = min;
        this.max = max;
    }

    public static void saveRule(String mark, double[] min, double[] max) {
        Document doc;
        doc = new Document();
        doc.append("mark", mark);
        doc.append("min1", min[0]);
        doc.append("min2", min[1]);
        doc.append("min3", min[2]);
        doc.append("min4", min[3]);

        doc.append("max1", max[0]);
        doc.append("max2", max[1]);
        doc.append("max3", max[2]);
        doc.append("max4", max[3]);

        MongoConnection.getInstance()
                .getCollection("rules")
                .insertOne(doc);
    }

    public static Rule loadRule(String mark) {

        MongoCursor<Document> cursor = MongoConnection
                .getInstance()
                .getCollection("rules")
                .find(new Document("mark", mark))
                .iterator();
        Rule rule = null;
        try {
            while (cursor.hasNext()) {
                Document doc = cursor.next();
                        double[] min = new double[] {
                            doc.getDouble("min1"),
                            doc.getDouble("min2"),
                            doc.getDouble("min3"),
                            doc.getDouble("min4")
                        };
                        double[] max = new double[] {
                            doc.getDouble("max1"),
                            doc.getDouble("max2"),
                            doc.getDouble("max3"),
                            doc.getDouble("max4"),
                        };
                        rule = new Rule(
                            min, max
                        );
            }
        } finally {
            cursor.close();
        }
        return rule;
    }

    public boolean accepts(TCPPacketWindow window) {
        TCPPacketWindow minWindow, maxWindow;
        minWindow = new TCPPacketWindow();
        maxWindow = new TCPPacketWindow();

        minWindow.setPoint(getMin());
        maxWindow.setPoint(getMax());

        if (window.compareTo(minWindow) == -1 &&
                window.compareTo(maxWindow) == 1) {
            return true;
        }
        return false;
    }

}
