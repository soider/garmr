package edu.msahnov.garmr.analyze;

import edu.msahnov.garmr.sniffer.packets.TCPPacketMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by msahnov on 30.05.15.
 */
public class DataNormalizer {


    private ArrayList<TCPPacketWindow> windowsList;

    public void setWindowsList(ArrayList<TCPPacketWindow> windowsList) {
        this.windowsList = windowsList;
    }

    public ArrayList<TCPPacketWindow> getWindowsList() {
        return windowsList;
    }

    public void normalize(double newMin, double newMax) {
        double currentMin, currentMax;
        for (String key: TCPPacketMeta.getKeys()) {
            currentMin = findMinFor(key);
            currentMax = findMaxFor(key);
            for (TCPPacketWindow window: windowsList) {
                Double currentValue = window.fieldsEntropy.get(key);
                Double newValue;
                newValue = ((currentValue - currentMin)/(currentMax - currentMin)) * (newMax - newMin) + newMin;
                if (newValue.isNaN()) {
                    newValue = (double) 0;
                }
                window.fieldsEntropy.put(key, newValue);
            }
        }
    }

    private double findMinFor(String key) {
        double ret = (double) 0;
        for (TCPPacketWindow window: windowsList) {
            if (window.fieldsEntropy.get(key) < ret) {
                ret = window.fieldsEntropy.get(key);
            }
        }
        return ret;
    }

    private double findMaxFor(String key) {
        double ret = (double) 0;
        for (TCPPacketWindow window: windowsList) {
            if (window.fieldsEntropy.get(key) > ret) {
                ret = window.fieldsEntropy.get(key);
            }
        }
        return ret;
    }
}
